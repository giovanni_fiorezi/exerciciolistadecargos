package br.com.projeto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.magnasistemas.RetornaLista.Funcionario;
import br.com.magnasistemas.RetornaLista.Funcionarios;

public class TesteFuncionarioLimpo {
	
	public static void main(String[] args) {
		
		List<Funcionario> trainees = new ArrayList<Funcionario>();
		List<Funcionario> juniors = new ArrayList<Funcionario>();
		List<Funcionario> plenos = new ArrayList<Funcionario>();
		List<Funcionario> seniors = new ArrayList<Funcionario>();
		List<Funcionario> arquitetos = new ArrayList<Funcionario>();
		
		for (int i = 0; i < Funcionarios.retornaFuncionarios().size(); i++) {
			Funcionario funcionario = Funcionarios.retornaFuncionarios().get(i);
			
			if (funcionario.getCargo().equals("Trainee")) {
				trainees.add(funcionario);
			}
			if (funcionario.getCargo().equals("Junior")) {
				juniors.add(funcionario);
			}
			if (funcionario.getCargo().equals("Pleno")) {
				plenos.add(funcionario);
			}
			if (funcionario.getCargo().equals("Senior")) {
				seniors.add(funcionario);
			}
			if (funcionario.getCargo().equals("Arquiteto")) {
				arquitetos.add(funcionario);
			}
		}
		
		System.out.println("TRAINEES");
		trainees.sort((Funcionario f1, Funcionario f2) -> f1.getNome().compareTo(f2.getNome()));
		double valorTotalTrainees = 0;
		for (int i = 0; i < trainees.size(); i++) {
			Funcionario ft = trainees.get(i);
			System.out.println(ft.getNome() + " - " + "R$ " + somaSalarioEBonificacao(ft));
			valorTotalTrainees += somaSalarioEBonificacao(ft);
		}
		System.out.println("Total: " + "R$ " + valorTotalTrainees  + "\n");
	
		 	
		System.out.println("JUNIORS");
		juniors.sort((Funcionario f1, Funcionario f2) -> f1.getNome().compareTo(f2.getNome()));
		double valorTotalJuniors = 0;
		for (int i = 0; i < juniors.size(); i++) {
			Funcionario fj = juniors.get(i);
			System.out.println(fj.getNome() + " - " + "R$ " + somaSalarioEBonificacao(fj));
			valorTotalJuniors += somaSalarioEBonificacao(fj);
		}
		System.out.println("Total: " + "R$ " + valorTotalJuniors + "\n");
		
			
		System.out.println("PLENOS");
		plenos.sort((Funcionario f1, Funcionario f2) -> f1.getNome().compareTo(f2.getNome()));
		double valorTotalPlenos = 0;
		for (int i = 0; i < plenos.size(); i++) {
			Funcionario fp = plenos.get(i);
			System.out.println(fp.getNome() + " - " + "R$ " + somaSalarioEBonificacao(fp));
			valorTotalPlenos += somaSalarioEBonificacao(plenos.get(i));
		}	
		System.out.println("Total: " + "R$ " + valorTotalPlenos + "\n");
		
			
		System.out.println("SENIORS");
		seniors.sort((Funcionario f1, Funcionario f2) -> f1.getNome().compareTo(f2.getNome()));
		double valorTotalSeniors = 0;
		for (int i = 0; i < seniors.size(); i++) {
			Funcionario fs = seniors.get(i);
			System.out.println(fs.getNome() + " - " + "R$ " + somaSalarioEBonificacao(fs));
			valorTotalSeniors += somaSalarioEBonificacao(seniors.get(i));
		}	
		System.out.println("Total: " + "R$ " + valorTotalSeniors + "\n");
		
			
		System.out.println("ARQUITETOS");
		arquitetos.sort((Funcionario f1, Funcionario f2) -> f1.getNome().compareTo(f2.getNome()));
		double valorTotalArquitetos = 0;
		for (int i = 0; i < arquitetos.size(); i++) {
			Funcionario fa = arquitetos.get(i);
			System.out.println(fa.getNome() + " - " + "R$ " + somaSalarioEBonificacao(fa));
			valorTotalArquitetos += somaSalarioEBonificacao(arquitetos.get(i));
		}
		System.out.println("Total: " + "R$ " + valorTotalArquitetos+ "\n");
	}
	
	
	public static double somaSalarioEBonificacao(Funcionario f) {
		return f.getSalario() + f.getBeneficios();
	}
}